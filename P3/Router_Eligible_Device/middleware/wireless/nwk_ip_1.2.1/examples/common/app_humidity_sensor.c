/*
 * Copyright (c) 2014 - 2015, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*!=================================================================================================
\file       app_humidity_sensor.c
\brief      This is a public source file for the application temperature sensor.
==================================================================================================*/

/*==================================================================================================
Include Files
==================================================================================================*/

/* General Includes */
#include "EmbeddedTypes.h"
#include "network_utils.h"
#include "stdlib.h"

/* Drivers */
#include "board.h"
#include "fsl_adc16.h"
#include "fsl_pmc.h"

#include "app_humidity_sensor.h"
#include "FunctionLib.h"
#include "MemManager.h"

/*==================================================================================================
Private macros
==================================================================================================*/

#define HUM_BUFF_SIZE     (20U)
/*==================================================================================================
Private global variables declarations
==================================================================================================*/
static uint32_t humidity_sensor = 50;

/*==================================================================================================
Public global variables declarations
==================================================================================================*/

/* None */

/*==================================================================================================
Private prototypes
==================================================================================================*/

/*==================================================================================================
Public functions
==================================================================================================*/
int32_t App_GetSimulatedHumidityData(int32_t current_value, int32_t max_step, int32_t min_value, int32_t max_value, bool increase)
{
	uint8_t coin[1];
	uint8_t step[1];
	NWKU_GenRand(coin,1);
	NWKU_GenRand(step,1);
	// flip the coin to see if the value increases o decreases:
	coin[0] = coin[0]%100;
	step[0] = (int32_t)step[0]%max_step;
	if(coin[0] > (increase?30:70)){
		if((current_value + step[0]) <= max_value) {
			current_value += step[0];
		}
		else {
			current_value = max_value;
		}
	}
	else {
		if((current_value - step[0]) >= min_value) {
			current_value -= step[0];
		}
		else {
			current_value = min_value;
		}
	}
	return current_value;
}

void* App_GetHumidityDataString(void)
{
	uint8_t* pIndex = NULL;
	uint8_t sHum[] = "Hum:";
	uint8_t * sendHumidityData = MEM_BufferAlloc(HUM_BUFF_SIZE);
	if(NULL == sendHumidityData)
	{
		return sendHumidityData;
	}
	/* Compute Humidity */
	humidity_sensor = App_GetSimulatedHumidityData(humidity_sensor, 2, 10, 100, 1);
	/* Clear data and reset buffers */
	FLib_MemSet(sendHumidityData, 0, HUM_BUFF_SIZE);

	/* Compute output */
	pIndex = sendHumidityData;
	FLib_MemCpy(pIndex, sHum, SizeOfString(sHum));
	pIndex += SizeOfString(sHum);
	NWKU_PrintDec((uint8_t)(abs(humidity_sensor)%100), pIndex, 2, TRUE);
	pIndex += 2; /* keep only the first 2 digits */
	*pIndex = '%';
	return sendHumidityData;
}
/*===============================================================================================
Private debug functions
==================================================================================================*/

