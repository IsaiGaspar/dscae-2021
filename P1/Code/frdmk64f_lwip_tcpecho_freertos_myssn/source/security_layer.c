/**
 ** @file:   security_layer.c
 ** @brief:  This file contains the implementation of a secure layer in
 **          a TCP/IP transmission/reception.
 **          This layer sends and receives packets through TCP using AES
 **          and CRC32 so the data can be encrypted, decrypted and
 **          verified for consistency.
 ** @since:  Feb 16, 2021
 ** @author: Isai Gaspar
 **/
/*******************************************************************************
 *  (c) 2021 ITESO, Universidad Jesuita de Guadalajara
 ******************************************************************************/

/* Header includes */
#include "aes.h"
#include "fsl_crc.h"

#include "tcpecho.h"
#include "lwip/netifapi.h"
#include "lwip/tcpip.h"
#include "netif/ethernet.h"
#include "enet_ethernetif.h"

#include "security_layer.h"


/*******************************************************************************
 *  Local variable  definitions
 ******************************************************************************/


/*******************************************************************************
 *  Global functions prototypes
 ******************************************************************************/
uint32_t Security_Calculate_CRC(uint8_t * buffer, uint8_t length);
bool Security_CRC_Check (uint8_t * buffer, uint8_t length);
void Security_CRC_Set (uint8_t * buffer, uint8_t length);

uint16_t Security_AES_Encrypt(uint8_t * data_buffer, uint8_t length);
void Security_AES_Decrypt(uint8_t * data_buffer, uint8_t length);

/*******************************************************************************
 *  Local functions prototypes
 ******************************************************************************/
static void InitCrc32(CRC_Type *base, uint32_t seed);

/*******************************************************************************
 *  Function definitions
 ******************************************************************************/

/* @brief Init for CRC-32.
 * @details Init CRC peripheral module for CRC-32 protocol.
 *          width=32 poly=0x04c11db7 init=0xffffffff refin=true refout=true
 *          xorout=0xffffffff check=0xcbf43926
 *          name="CRC-32"
 *          http://reveng.sourceforge.net/crc-catalogue/
 */
static void InitCrc32(CRC_Type *base, uint32_t seed)
{
    crc_config_t config;

    config.polynomial         = 0x04C11DB7U;
    config.seed               = seed;
    config.reflectIn          = true;
    config.reflectOut         = true;
    config.complementChecksum = true;
    config.crcBits            = kCrcBits32;
    config.crcResult          = kCrcFinalChecksum;

    CRC_Init(base, &config);
}

/* Function: Security_Calculate_CRC*/
/** Calculates CRC32 with a given buffer and a length of such buffer
 **
 ** @param[in]  buffer - Input buffer which contains the data to be used for CRC
 **             length - Total length of the buffer in bytes to calculate CRC
 **
 ** @return:    uint32_t - CRC32 calculated value
 **
 */
uint32_t Security_Calculate_CRC(uint8_t * buffer, uint8_t length)
{
	/* CRC data */
	CRC_Type *base = CRC0;
	uint32_t checksum32;

    InitCrc32(base, 0xFFFFFFFFU);
    CRC_WriteData(base, buffer, length);
    checksum32 = CRC_Get32bitResult(base);

    return checksum32;
}

/* Function: Security_CRC_Check*/
/** Checks the integrity of the received packet in a given buffer
 **
 ** The buffer must contain the CRC32 calculation at the end of it (last 4 bytes)
 ** The endiannesss of the CRC32 must be little endian.
 **
 ** @param[in]  buffer - Input buffer which contains the data to be checked
 **             length - Total length of the buffer including the CRC data
 **
 ** @return:    bool - true : Data is consistent
 **                    false: Data is inconsistent
 **
 */
bool Security_CRC_Check (uint8_t * buffer, uint8_t length)
{
	uint32_t checksum_in_packet, checksum_in_data;
	uint8_t * checksum_ptr = (uint8_t *)&checksum_in_packet;
	bool checksum_equal = false;

	checksum_in_data = Security_Calculate_CRC(buffer, length - 4U);

	checksum_ptr[0] = buffer[length-4];
	checksum_ptr[1] = buffer[length-3];
	checksum_ptr[2] = buffer[length-2];
	checksum_ptr[3] = buffer[length-1];

	if(checksum_in_packet == checksum_in_data)
	{
		checksum_equal = true;
	}

	return checksum_equal;
}

/* Function: Security_CRC_Set*/
/** Adds the CRC32 at the end of a given buffer
 **
 ** The buffer must have 4 bytes of space free at the end of it. The CRC will
 ** be stored there.
 **
 ** @param[in]  buffer - Input buffer which contains the data that will be
 **                      used to calculate the CRC32
 **             length - Total length of the buffer
 **
 ** @return:    void
 **
 */
void Security_CRC_Set(uint8_t * buffer, uint8_t length)
{
	uint32_t  checksum;
	uint8_t * checksum_ptr = (uint8_t *)&checksum;

	checksum = Security_Calculate_CRC(buffer, length - 4U);

	buffer[length-4] = checksum_ptr[0];
	buffer[length-3] = checksum_ptr[1];
	buffer[length-2] = checksum_ptr[2];
	buffer[length-1] = checksum_ptr[3];

}

/* Function: Security_AES_Encrypt*/
/** Encrypts a buffer with AES
 **
 ** The buffer must have a size of a multiple of 16. So it is expected
 ** that the input value "length" is a multiple of 16.
 **
 ** @param[in]  data_buffer - Input buffer which contains the data to be encrypted
 **             length - Total length of the buffer to be used.
 **
 ** @return:    New padded length
 **
 */
uint16_t Security_AES_Encrypt(uint8_t * data_buffer, uint8_t length)
{
	/* AES data */
	struct AES_ctx ctx;
	size_t  padded_len;
	uint8_t key[] = AES_KEY;
	uint8_t iv[] = AES_INIT_VECTOR;

	/** 1) Init the AES context structure **/
	AES_init_ctx_iv(&ctx, key, iv);

	/** 2) To encrypt an array its length must be a multiple of 16 so we add zeros*/
	padded_len = length + (16 - (length%16) );

	/** 3) Encrypt buffer, data_buffer will now have encrypted data inside it **/
	AES_CBC_encrypt_buffer(&ctx, data_buffer, padded_len);

	return padded_len;

}

/* Function: Security_AES_Decrypt*/
/** Decrypts a buffer with AES
 **
 **
 ** @param[in]  buffer - Input buffer which contains the data to be decrypted
 **             length - Total length of the buffer to be used, this length
 **                      must be a multiple of 16.
 **
 ** @return:    void
 **
 ** @note: After decrypting the message there may be elements of the buffer
 **        containing 0's because of the padding.
 ** @steps:
 */
void Security_AES_Decrypt(uint8_t * data_buffer, uint8_t length)
{
	/* AES data */
	struct AES_ctx ctx;
	uint8_t key[] = AES_KEY;
	uint8_t iv[] = AES_INIT_VECTOR;
	/** 1) Init the AES context structure **/
	AES_init_ctx_iv(&ctx, key, iv);
	/** 3) Encrypt buffer, data_buffer will now have the actual data inside of it **/
	AES_CBC_decrypt_buffer(&ctx, data_buffer, length);
}
