/**
 ** @file:   security_layer.h
 ** @brief:  This file contains the APIs for a secure layer in
 **          a TCP/IP transmission/reception.
 ** @since:  Feb 16, 2021
 ** @author: Isai Gaspar
 **/
/*******************************************************************************
 *  (c) 2021 ITESO, Universidad Jesuita de Guadalajara
 ******************************************************************************/

#ifndef SECURITY_LAYER_H_
#define SECURITY_LAYER_H_


/*******************************************************************************
 *  Global macro definitions
 ******************************************************************************/

#define AES_KEY         {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,\
	                     0x09, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06 }
#define AES_INIT_VECTOR {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,\
	                     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

/*******************************************************************************
 *  Global Function definitions
 ******************************************************************************/

/* Function: Security_Calculate_CRC*/
/** Calculates CRC32 with a given buffer and a length of such buffer
 **
 ** @param[in]  buffer - Input buffer which contains the data to be used for CRC
 **             length - Total length of the buffer in bytes to calculate CRC
 **
 ** @return:    uint32_t - CRC32 calculated value
 **
 */
extern uint32_t Security_Calculate_CRC(uint8_t * buffer, uint8_t length);

/* Function: Security_CRC_Check*/
/** Checks the integrity of the received packet in a given buffer
 **
 ** The buffer must contain the CRC32 calculation at the end of it (last 4 bytes)
 ** The endiannesss of the CRC32 must be little endian.
 **
 ** @param[in]  buffer - Input buffer which contains the data to be checked
 **             length - Total length of the buffer including the CRC data
 **
 ** @return:    bool - true : Data is consistent
 **                    false: Data is inconsistent
 **
 */
extern bool Security_CRC_Check (uint8_t * buffer, uint8_t length);

/* Function: Security_CRC_Set*/
/** Adds the CRC32 at the end of a given buffer
 **
 ** The buffer must have 4 bytes of space free at the end of it. The CRC will
 ** be stored there.
 **
 ** @param[in]  buffer - Input buffer which contains the data that will be
 **                      used to calculate the CRC32
 **             length - Total length of the buffer
 **
 ** @return:    void
 **
 */
extern void Security_CRC_Set (uint8_t * buffer, uint8_t length);

/* Function: Security_AES_Encrypt*/
/** Encrypts a buffer with AES
 **
 ** The buffer must have a size of a multiple of 16. So it is expected
 ** that the input value "length" is a multiple of 16.
 **
 ** @param[in]  data_buffer - Input buffer which contains the data to be encrypted
 **             length - Total length of the buffer to be used.
 **
 ** @return:    New padded length
 **
 */
extern uint16_t Security_AES_Encrypt(uint8_t * data_buffer, uint8_t length);

/* Function: Security_AES_Decrypt*/
/** Decrypts a buffer with AES
 **
 ** @param[in]  buffer - Input buffer which contains the data to be decrypted
 **             length - Total length of the buffer to be used, this length
 **                      must be a multiple of 16.
 **
 ** @return:    void
 **
 ** @note: After decrypting the message there may be elements of the buffer
 **        containing 0's because of the padding.
 **
 */
extern void Security_AES_Decrypt(uint8_t * data_buffer, uint8_t length);



#endif /* SECURITY_LAYER_H_ */
