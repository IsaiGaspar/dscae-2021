/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2019 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "lwip/opt.h"

#if LWIP_NETCONN

#include "tcpecho.h"
#include "lwip/netifapi.h"
#include "lwip/tcpip.h"
#include "netif/ethernet.h"
#include "enet_ethernetif.h"

#include "board.h"

#include "fsl_device_registers.h"
#include "pin_mux.h"
#include "clock_config.h"

#include "aes.h"
#include "fsl_crc.h"

#include "security_layer.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/

/* IP address configuration. */
#define configIP_ADDR0 192
#define configIP_ADDR1 168
#define configIP_ADDR2 0
#define configIP_ADDR3 102

/* Netmask configuration. */
#define configNET_MASK0 255
#define configNET_MASK1 255
#define configNET_MASK2 255
#define configNET_MASK3 0

/* Gateway address configuration. */
#define configGW_ADDR0 192
#define configGW_ADDR1 168
#define configGW_ADDR2 0
#define configGW_ADDR3 100

/* MAC address configuration. */
#define configMAC_ADDR                     \
    {                                      \
        0x02, 0x12, 0x13, 0x10, 0x15, 0x11 \
    }

/* Address of PHY interface. */
#define EXAMPLE_PHY_ADDRESS BOARD_ENET0_PHY_ADDRESS

/* System clock name. */
#define EXAMPLE_CLOCK_NAME kCLOCK_CoreSysClk


#ifndef EXAMPLE_NETIF_INIT_FN
/*! @brief Network interface initialization function. */
#define EXAMPLE_NETIF_INIT_FN ethernetif0_init
#endif /* EXAMPLE_NETIF_INIT_FN */

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
void aescrc_rx_task(void *arg);
void aescrc_tx_task(void *arg);
/*******************************************************************************
 * Variables
 ******************************************************************************/
static uint8_t tcp_app_data_buffer[512] = {0};
/*******************************************************************************
 * Code
 ******************************************************************************/

/*!
 * @brief Main function
 */
int main(void)
{
    static struct netif netif;
#if defined(FSL_FEATURE_SOC_LPC_ENET_COUNT) && (FSL_FEATURE_SOC_LPC_ENET_COUNT > 0)
    static mem_range_t non_dma_memory[] = NON_DMA_MEMORY_ARRAY;
#endif /* FSL_FEATURE_SOC_LPC_ENET_COUNT */
    ip4_addr_t netif_ipaddr, netif_netmask, netif_gw;
    ethernetif_config_t enet_config = {
        .phyAddress = EXAMPLE_PHY_ADDRESS,
        .clockName  = EXAMPLE_CLOCK_NAME,
        .macAddress = configMAC_ADDR,
#if defined(FSL_FEATURE_SOC_LPC_ENET_COUNT) && (FSL_FEATURE_SOC_LPC_ENET_COUNT > 0)
        .non_dma_memory = non_dma_memory,
#endif /* FSL_FEATURE_SOC_LPC_ENET_COUNT */
    };

    SYSMPU_Type *base = SYSMPU;
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();
    /* Disable SYSMPU. */
    base->CESR &= ~SYSMPU_CESR_VLD_MASK;

    IP4_ADDR(&netif_ipaddr, configIP_ADDR0, configIP_ADDR1, configIP_ADDR2, configIP_ADDR3);
    IP4_ADDR(&netif_netmask, configNET_MASK0, configNET_MASK1, configNET_MASK2, configNET_MASK3);
    IP4_ADDR(&netif_gw, configGW_ADDR0, configGW_ADDR1, configGW_ADDR2, configGW_ADDR3);

    tcpip_init(NULL, NULL);

    netifapi_netif_add(&netif, &netif_ipaddr, &netif_netmask, &netif_gw, &enet_config, EXAMPLE_NETIF_INIT_FN,
                       tcpip_input);
    netifapi_netif_set_default(&netif);
    netifapi_netif_set_up(&netif);

    PRINTF("\r\n************************************************\r\n");
    PRINTF(" TCP Echo example\r\n");
    PRINTF("************************************************\r\n");
    PRINTF(" IPv4 Address     : %u.%u.%u.%u\r\n", ((u8_t *)&netif_ipaddr)[0], ((u8_t *)&netif_ipaddr)[1],
           ((u8_t *)&netif_ipaddr)[2], ((u8_t *)&netif_ipaddr)[3]);
    PRINTF(" IPv4 Subnet mask : %u.%u.%u.%u\r\n", ((u8_t *)&netif_netmask)[0], ((u8_t *)&netif_netmask)[1],
           ((u8_t *)&netif_netmask)[2], ((u8_t *)&netif_netmask)[3]);
    PRINTF(" IPv4 Gateway     : %u.%u.%u.%u\r\n", ((u8_t *)&netif_gw)[0], ((u8_t *)&netif_gw)[1],
           ((u8_t *)&netif_gw)[2], ((u8_t *)&netif_gw)[3]);
    PRINTF("************************************************\r\n");


    sys_thread_new("aescrc_rx_task", aescrc_rx_task, NULL, 1024, 4);

    vTaskStartScheduler();

    /* Will not get here unless a task calls vTaskEndScheduler ()*/
    return 0;
}
#endif



/* Function: aescrc_rx_task*/
/** Receive data through TCP and decrypt it using AES and checking its CRC32
 **
 ** This task follows the typical server socket connection on TCP/IP:
 ** +--------+  +------+  +--------+  +--------+  +------+  +-------+  +-------+
 ** |socket()+->+bind()+->+listen()+->+accept()+->+read()+->+write()+->+close()+
 ** +--------+  +------+  +---+----+  +--+-----+  +------+  +-------+  +-------+
 **                                                  |          |
 **                                                  v          v
 **                                            Data(Request) Data(Reply)
 **                                          +--------------------------+
 **                                                     Client
 **
 ** @b Steps:
 */
void aescrc_rx_task(void *arg)
{

	/*LW IP data*/
	struct netconn *conn, *newconn;
	err_t err;
	while (1)
	{
		/** 1) Create a new connection identifier. **/
		conn = netconn_new(NETCONN_TCP);

		/** 2) Bind connection to well known port number 7. **/
		netconn_bind(conn, IP_ADDR_ANY, 7);

		LWIP_ERROR("tcp_app: invalid conn", (conn != NULL), return ;);

		/** 3) Tell connection to go into listening mode. **/
		netconn_listen(conn);

		/** 4) Grab new connection. **/
		err = netconn_accept(conn, &newconn);

		/** 5) Process the new connection. **/
		if (err == ERR_OK)
		{
			PRINTF("Connected! %p\r\n", newconn);
			struct netbuf *buf;
			void *data;
			u16_t len;
			uint16_t padded_len;

			while ((err = netconn_recv(newconn, &buf)) == ERR_OK)
			{
				do {
					netbuf_data(buf, &data, &len);

					/**5.1) Process the received data **/
					memcpy(tcp_app_data_buffer, data, len);

					if(true == Security_CRC_Check(tcp_app_data_buffer, len))
					{
						PRINTF("Checksum is correct \r\n");
					}
					else
					{
						PRINTF("ERROR: Checksum is inconsistent \r\n");
					}
					/**5.2 Decrypt data**/
					Security_AES_Decrypt(tcp_app_data_buffer, len - 4);
					PRINTF("Received: \r\n");
					/*Set null character and print received data as string*/
					tcp_app_data_buffer[len]   = 0;
					PRINTF("%s\r\n", tcp_app_data_buffer);

					/** 5.3) Encrypt data again to test the functions Encrypt and Set CRC **/
					padded_len = Security_AES_Encrypt(tcp_app_data_buffer, len - 4U);

					/** 5.4) Add CRC to the data buffer **/
					Security_CRC_Set(tcp_app_data_buffer, padded_len + 4);

					memcpy(data, tcp_app_data_buffer, padded_len + 4);
					/**5.5) Send encrypted data to the client**/
					err = netconn_write(newconn, data, padded_len + 4, NETCONN_COPY);

					if (err != ERR_OK) {
						PRINTF("tcp_app: netconn_write: error \"%s\"\n", lwip_strerr(err));
					}

				} while (netbuf_next(buf) >= 0);
				netbuf_delete(buf);
			}
			/** 6) Close connection and discard connection identifier **/
			PRINTF("Connection closed\n");
			netconn_close(newconn);
			netconn_delete(newconn);
		}
	}
}
